﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour
{
    /// <summary>
    /// Points to draw lines.
    /// </summary>
    List<Vector2> m_lstPoints;

    /// <summary>
    /// Default line color.
    /// </summary>
    [SerializeField]
    Color m_lineColor = Color.red;

    /// <summary>
    /// Assigns points to draw lines.
    /// </summary>
    /// <param name="a_lstPoints">Points.</param>
    internal void AssignPoints(List<Vector2> a_lstPoints)
    {
        m_lstPoints = a_lstPoints;
    }

    /// <summary>
    /// Update lines.
    /// </summary>
    internal void Update()
    {
        int l_index = 0;
        do
        {
            int l_indexNext = (l_index + 1) % m_lstPoints.Count;

            Debug.DrawLine(m_lstPoints[l_index], m_lstPoints[l_indexNext], m_lineColor);

            l_index = l_indexNext;
        } while (l_index != 0);
    }

    /// <summary>
    /// Change line color.
    /// </summary>
    /// <param name="a_color">New color.</param>
    internal void SetColor(Color a_color)
    {
        m_lineColor = a_color;
    }
}