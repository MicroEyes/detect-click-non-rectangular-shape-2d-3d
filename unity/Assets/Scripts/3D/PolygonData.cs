﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolygonData : MonoBehaviour
{
    [SerializeField]
    List<Transform> m_lstTransform;

    public List<Vector2> GetPoints()
    {
        List<Vector2> l_lstPoints = new List<Vector2>();

        foreach (Transform l_transform in m_lstTransform)
        {
            l_lstPoints.Add(l_transform.position);
        }

        return l_lstPoints;
    }
}
