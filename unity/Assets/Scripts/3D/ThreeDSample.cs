﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThreeDSample : MonoBehaviour
{
    /// <summary>
    /// Polygon data reference.
    /// </summary>
    [SerializeField]
    PolygonData m_polygonData = null;

    /// <summary>
    /// Line drawer reference.
    /// </summary>
    [SerializeField]
    LineDrawer m_lineDrawer = null;

    [SerializeField]
    Transform m_tranformPointToCheck;

    /// <summary>
    /// Runtime data for PointToCheck.
    /// </summary>
    Vector2 m_pointToCheck;
    
    /// <summary>
    /// Runtime data for Point.
    /// </summary>
    List<Vector2> m_lstPoints;

    // Use this for initialization
    void Start()
    {
        m_lstPoints = m_polygonData.GetPoints();
        m_lineDrawer.AssignPoints(m_lstPoints);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        m_pointToCheck = m_tranformPointToCheck.position;

        Color l_color = Color.red;
        if (PolygonUtil.IsInside(m_lstPoints, m_pointToCheck)) //Changing line color if points is inside polygon.
            l_color = Color.green;

        m_lineDrawer.SetColor(l_color);

        //Update line.
        if (m_lineDrawer)
            m_lineDrawer.Update();
    }
}