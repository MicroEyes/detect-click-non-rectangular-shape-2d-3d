﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PolygonUtil
{
    const float MAX_RIGHT_POSITION = 1000.0f;
    public static bool IsInside(List<Vector2> a_lstPolygonPoints, Vector2 a_pointToCheck)
    {
        if (!IsPolygon(a_lstPolygonPoints))
            return false;

        //Most right-side position
        Vector2 l_pointToMostRight = new Vector2(MAX_RIGHT_POSITION, a_pointToCheck.y);

        //Counter for number of intersection with polygon sides.
        int l_count = 0;

        int l_index = 0;

        do
        {
            int l_indexNext = (l_index + 1) % a_lstPolygonPoints.Count;
            if (IsIntersecting(a_lstPolygonPoints[l_index], a_lstPolygonPoints[l_indexNext], a_pointToCheck, l_pointToMostRight))
            {
                if (CheckColinear(a_lstPolygonPoints[l_index], a_pointToCheck, a_lstPolygonPoints[l_indexNext]) == 0)
                    return CheckOnLineSegment(a_lstPolygonPoints[l_index], a_pointToCheck, a_lstPolygonPoints[l_indexNext]);

                l_count++;
            }


            l_index = l_indexNext;
        } while (l_index != 0);



        return (l_count % 2 == 1); //TRUE if odd.
    }

    /// <summary>
    /// Checking if two Line(a_pointOnPolygon, a_nextPointOnPolygon) is intersecting Line(a_pointToCheck, a_pointToMostRight)
    /// </summary>
    /// <param name="a_pointOnPolygon"></param>
    /// <param name="a_nextPointOnPolygon"></param>
    /// <param name="a_pointToCheck"></param>
    /// <param name="a_pointToMostRight"></param>
    /// <returns></returns>
    private static bool IsIntersecting(Vector2 a_pointOnPolygon, Vector2 a_nextPointOnPolygon, Vector2 a_pointToCheck, Vector2 a_pointToMostRight)
    {
        int l_value1 = CheckColinear(a_pointOnPolygon, a_nextPointOnPolygon, a_pointToCheck);
        int l_value2 = CheckColinear(a_pointOnPolygon, a_nextPointOnPolygon, a_pointToMostRight);
        int l_value3 = CheckColinear(a_pointToCheck, a_pointToMostRight, a_pointOnPolygon);
        int l_value4 = CheckColinear(a_pointToCheck, a_pointToMostRight, a_nextPointOnPolygon);

        // General case 
        if (l_value1 != l_value2 && l_value3 != l_value4)
            return true;

        // Special Cases 
        // p1, q1 and p2 are colinear and p2 lies on segment p1q1 
        if (l_value1 == VALUE__COLINEAR && CheckOnLineSegment(a_pointOnPolygon, a_pointToCheck, a_nextPointOnPolygon)) return true;

        // p1, q1 and p2 are colinear and q2 lies on segment p1q1 
        if (l_value2 == VALUE__COLINEAR && CheckOnLineSegment(a_pointOnPolygon, a_pointToMostRight, a_nextPointOnPolygon)) return true;

        // p2, q2 and p1 are colinear and p1 lies on segment p2q2 
        if (l_value3 == VALUE__COLINEAR && CheckOnLineSegment(a_pointToCheck, a_pointOnPolygon, a_pointToMostRight)) return true;

        // p2, q2 and q1 are colinear and q1 lies on segment p2q2
        if (l_value4 == VALUE__COLINEAR && CheckOnLineSegment(a_pointToCheck, a_nextPointOnPolygon, a_pointToMostRight)) return true;

        return false;
    }

    const int VALUE__COLINEAR = 0;
    const int VALUE__CLOCKWISE = 1;
    const int VALUE__COUNTER_CLOCKWISE = 2;

    private static int CheckColinear(Vector2 a_p1, Vector2 a_p2, Vector2 a_p3)
    {
        //Checking slope of p1,p2 and p2
        float l_value = ((a_p2.y - a_p1.y) * (a_p3.x - a_p2.x)) - ((a_p2.x - a_p1.x) * (a_p3.y - a_p2.y));
        if (Mathf.Approximately(l_value, 0))
            return VALUE__COLINEAR;
        else if (l_value > 0)
            return VALUE__CLOCKWISE;
        else
            return VALUE__COUNTER_CLOCKWISE;

    }

    /// <summary>
    /// Checking if point a_p2 lies on line segment 'a_p1,a_p3'.
    /// </summary>
    /// <param name="a_p1"></param>
    /// <param name="a_p2"></param>
    /// <param name="a_p3"></param>
    /// <returns></returns>
    private static bool CheckOnLineSegment(Vector2 a_p1, Vector2 a_p2, Vector2 a_p3)
    {
        if (a_p2.x <= Mathf.Max(a_p1.x, a_p3.x) && a_p2.x >= Mathf.Min(a_p1.x, a_p3.x) &&
                a_p2.y <= Mathf.Max(a_p1.y, a_p3.y) && a_p2.y >= Mathf.Min(a_p1.y, a_p3.y))
            return true;
        return false;
    }

    private static bool IsPolygon(List<Vector2> a_lstPolygonPoints)
    {
        //Need atleast 3 vertices to be a polygon.
        return a_lstPolygonPoints.Count >= 3;
    }
}