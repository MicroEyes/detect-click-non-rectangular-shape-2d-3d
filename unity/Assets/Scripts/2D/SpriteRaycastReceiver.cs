﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteRaycastReceiver : MonoBehaviour, ICanvasRaycastFilter
{
    [SerializeField]
    RectTransform m_rect;

    [SerializeField]
    Image m_imgRef = null;
    List<Vector2> m_lstPoints;
    const int MULTIPLIER = 100;
    private void Start()
    {
        Debug.Log("Sprite Dimension: " + m_imgRef.sprite.texture.width + ", " + m_imgRef.sprite.texture.height);

        float l_textureScaleFactor = m_rect.sizeDelta.y / m_imgRef.sprite.texture.height;
        Debug.Log("l_textureScaleFactor: " + l_textureScaleFactor);

        List<Vector2> l_lstPointsInTextureSpace = new List<Vector2>();
        int l_numGetPhysicsShape = m_imgRef.sprite.GetPhysicsShape(0, l_lstPointsInTextureSpace);
        Debug.Log("l_numGetPhysicsShape: " + l_numGetPhysicsShape);

        Transform l_parentRect = transform.parent;
        m_lstPoints = new List<Vector2>(l_lstPointsInTextureSpace.Count);

        foreach (Vector2 l_vec in l_lstPointsInTextureSpace)
        {
            //Reverting back Unity's doing by multiplying 100. 'Sprite.GetPhysicsShape()' returns points divided by 100.
            Vector2 l_vecMultiplied = new Vector2(l_vec.x * MULTIPLIER, l_vec.y * MULTIPLIER);

            //Texture space position in texture w.r.t Image.transform.sizeDelta.
            Vector2 l_pixelPositionScaled = l_vecMultiplied * l_textureScaleFactor;

            //Position of texture space to canvas space.
            Vector2 l_canvasSpacePosition = new Vector2(m_rect.anchoredPosition.x + l_pixelPositionScaled.x, m_rect.anchoredPosition.y + l_pixelPositionScaled.y);

            //Canvas space position to World position.
            Vector3 l_worldPosition = l_parentRect.TransformPoint(l_canvasSpacePosition);

            //World space to screen space.
            Vector3 l_screenPosition = RectTransformUtility.WorldToScreenPoint(null, l_worldPosition);

            m_lstPoints.Add(l_screenPosition);
        }
    }

    public bool IsRaycastLocationValid(Vector2 a_screenPosition, Camera eventCamera)
    {
        if (PolygonUtil.IsInside(m_lstPoints, a_screenPosition))
            return true;

        return false;
    }
}