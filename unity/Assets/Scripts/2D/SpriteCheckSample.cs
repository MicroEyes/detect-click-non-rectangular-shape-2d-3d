﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class to receive callback, when clicked inside the Shape.
/// </summary>
public class SpriteCheckSample : MonoBehaviour
{
    [SerializeField]
    private Canvas m_canvasRef = null;
    
    RectTransform m_canvasTransRef = null;

    [SerializeField]
    Image m_imgMarker = null;

    private void Start()
    {
        m_canvasTransRef = m_canvasRef.transform as RectTransform;
    }

    /// <summary>
    /// called when clicked inside the PhysicsShape.
    /// </summary>
    public void OnClickedInsideImageShape()
    {
        InstantiateMarker();
    }

    /// <summary>
    /// Instantiate marker at mouse position.
    /// </summary>
    void InstantiateMarker()
    {
        Vector2 l_localPointInCanvas;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(m_canvasTransRef, Input.mousePosition, null, out l_localPointInCanvas);
        Image l_image = Instantiate(m_imgMarker, m_canvasTransRef);
        RectTransform l_transformNew = l_image.transform as RectTransform;
        l_transformNew.anchoredPosition = l_localPointInCanvas;
        l_image.gameObject.SetActive(true);
    }
}