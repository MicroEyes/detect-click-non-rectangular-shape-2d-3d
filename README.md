This Unity3D project to demonstrate detect UI pointer events on a non-rectangular sprite assigned to 'Image' component. This project is using math and not using any in-built functions for detection.

---

This Project contains two types of implementations:
## 3D World
This type was an intial implementation of detection of another object inside the polygon in 3D space.

Implemented in Scene *CheckPointInWorld*.

Instruction:

1. Created polygon using *Cube* primitives and place inside a parent GameObject say "parentGO".
2. Assign Component *PolygonData* to parentGO and assign cubes from #1 to make a polygon shape.
3. Assign *PolygonData* to GameObject *ThreeDSample* -> Component *ThreeDSample.PolygonData*.

Using LineDrawer to draw line among points to show the polygon.

GameObject *PointToCheck* gives the position to check whether inside polygon #3 above.

## 2D UI

Implemented in Scene *CheckPointOnSprite*.

**Instructions**:

1. Import any Texture and change *TextureType* to *Sprite*.
2. In *Texture Import Settings*, turn ON *Generate Physics*.
3. Using Unity's in-built *Sprite Editor*, create a *Custom Physics Shape* of a Sprite.
4. In SceneView, create Cubes (or any other object) to create Polygon. This project is using cube to create a polygon shape.
5. Assign sprite to any *Image* component and assign component "SpriteRaycastReceiver.cs" to *Image* in scene.
6. Assign events to the *Image* component.

---



**PolygonUtil.cs**: All the magic happens in this class. This *PolygonUtil* contains function with signature:

   *bool IsInside(List<Vector2> a_lstPolygonPoints, Vector2 a_pointToCheck)*

which returns *true* if *a_pointToCheck* is inside polygon *a_lstPolygonPoints*.
For polygon, we are considering only x-axis and y-axis for calculations.